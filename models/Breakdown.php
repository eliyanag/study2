<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use app\models\User;
use app\models\Level;
use app\models\Status;
/**
 * This is the model class for table "breakdown".
 *
 * @property integer $id
 * @property string $title
 * @property integer $levelId
 * @property integer $statusId
 */
class Breakdown extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'breakdown';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['levelId', 'statusId'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'levelId' => 'Level ID',
            'statusId' => 'Status ID',
        ];
    }
	
	public function getStatusItem()
    {
        return $this->hasOne(Status::className(), ['id' => 'statusId']);
    }	
	public function getLevelItem()
    {
        return $this->hasOne(Level::className(), ['id' => 'levelId']);
    }	
}
