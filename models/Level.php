<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "level".
 *
 * @property integer $id
 * @property string $name
 */
class Level extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'level';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
	
	public static function getLeveles()
	{
		$allLeveles = self::find()->all();
		$allLevelesArray = ArrayHelper::
					map($allLeveles, 'id', 'name');
		return $allLevelesArray;						
	}
	public static function getLevelesWithAllLeveles()
	{
		$allLeveles = self::getLeveles();
		$allLeveles[null] = 'All Leveles';
		$allLeveles = array_reverse ( $allLeveles, true );
		return $allLeveles;	
	}
}
