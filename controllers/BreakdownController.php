<?php

namespace app\controllers;

use Yii;
use app\models\Breakdown;
use app\models\BreakdownSearch;
use app\models\Status;
use app\models\Level;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * BreakdownController implements the CRUD actions for Breakdown model.
 */
class BreakdownController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
		'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'only' => ['index', 'view', 'create', 'update', 'delete'],
				'rules' => [
					// allow authenticated users
					[
						'actions' => ['create'],
						'allow' => true,
						'roles' => ['createBreakdown'],
					],
					[
						'actions' => ['delete'],
						'allow' => true,
						'roles' => ['deleteBreakdown'],
					],					
					[
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['indexBreakdown','@',],
					],	
					[
						'actions' => ['update'],
						'allow' => true,
						'roles' => ['updateBreakdown',],
					],
					[
						'actions' => ['view'],
						'allow' => true,
						'roles' => ['viewBreakdown', '@',],
					],					
				],
			],			
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Breakdown models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BreakdownSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'statuses' => Status::getStatusesWithAllStatuses(),
				'statusId' => $searchModel->statusId,
			'leveles' => Level::getLevelesWithAllLeveles(),
				'levelId' => $searchModel->levelId,
        ]);
    }

    /**
     * Displays a single Breakdown model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Breakdown model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Breakdown();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Breakdown model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Breakdown model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Breakdown model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Breakdown the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Breakdown::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
