<?php
namespace app\rbac;

use yii\rbac\Rule;
use app\models\User;
use app\models\Breakdown;
use yii\web\NotFoundHttpException;
use Yii; 

class UrgentBreakdownRule extends Rule
{

	public $name = 'UrgentBreakdownRule';

	public function execute($user, $item, $params)
		{	
			if(isset($user)){
				$currentUserRole = \Yii::$app->authManager->getRolesByUser($user);
				
				if($currentUserRole == 'manager')
					return true;
					
			}
		
			return false;
		}
}