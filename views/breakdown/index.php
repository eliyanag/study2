<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BreakdownSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Breakdowns';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="breakdown-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Breakdown', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            //'levelId',
			[
				'attribute' => 'levelId',
				'label' => 'Level',
				'value' => function($model){
							return $model->levelItem->name;
					},	
				
					'filter'=>Html::dropDownList('BreakdownSearch[levelId]', 
					$levelId, $leveles, ['class'=>'form-control']),		
				
			],
           // 'statusId',
			[
				'attribute' => 'statusId',
				'label' => 'Status',
				'value' => function($model){
							return $model->statusItem->name;
					},	
				'filter'=>Html::dropDownList('BreakdownSearch[statusId]', 
				$statusId, $statuses, ['class'=>'form-control']),					
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
